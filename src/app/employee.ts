export class Employee {
    id:number | undefined;
    name:String | undefined;
    dob:String | undefined;
    age:number | undefined;
    salary:number | undefined;
}
