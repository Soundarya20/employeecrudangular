import { Component, OnInit } from '@angular/core';
import {Employee} from '../employee';
import { EmployeeService } from '../employee.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  employee: Employee = new Employee();
  submitted = false;
  
  constructor(private employeeService: EmployeeService,
    private router: Router) { }

  ngOnInit(){
  }

  save() {
    this.employeeService
    .addEmployee(this.employee).subscribe(data=> {
      console.log(data)
      this.employee = new Employee();
      this.gotoList();
    }, 
      error => {
        return console.log(error);
      });
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  gotoList() {
    this.router.navigate(['/employeelist']);
  }
}
